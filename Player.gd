extends KinematicBody2D

var SPEED = 5000.0
var emote_timer
var tracks = preload("res://tracks.tscn")
var laugh_texture = preload("res://png/emotes/Vector/Style 3/emote_laugh.png")
var alert_texture = preload("res://png/emotes/Vector/Style 3/emote_exclamation.png")
var heart_texture = preload("res://png/emotes/Vector/Style 3/emote_heart.png")
var happy_texture = preload("res://png/emotes/Vector/Style 3/emote_faceHappy.png")
var money_texture = preload("res://png/emotes/Vector/Style 3/emote_cash.png")
var sad_texture = preload("res://png/emotes/Vector/Style 3/emote_faceSad.png")
var idea_texture = preload("res://png/emotes/Vector/Style 3/emote_idea.png")
var angry_texture = preload("res://png/emotes/Vector/Style 3/emote_faceAngry.png")
var sleeps_texture = preload("res://png/emotes/Vector/Style 3/emote_sleeps.png")
var question_texture = preload("res://png/emotes/Vector/Style 3/emote_question.png")
var nope_texture = preload("res://png/emotes/Vector/Style 3/emote_cross.png")

puppet var slave_position = Vector2()
puppet var slave_rotation = 0

func _ready():
    randomize()
    emote_timer = Timer.new()
    emote_timer.connect("timeout", self, "_hide_emote")
    emote_timer.one_shot = true
    emote_timer.wait_time = 5
    add_child(emote_timer)
    self.position = Vector2((randi() % 5) * 128 + 256, ((randi() % 5) * 128 + 256))
    if is_network_master():
        get_node("Camera2D").current = true
        get_node("Camera2D/CanvasLayer/HBoxContainer").visible = true
        get_node("Camera2D/CanvasLayer/HSplitContainer").visible = true
    
func _physics_process(delta):
    
    var velocity = Vector2()
    var movement_rotation = 0
    var my_speed = SPEED
    if is_network_master():
        if Input.is_action_pressed("shift"):
            my_speed = SPEED * 2
        
        if Input.is_action_pressed("ui_up"):
            velocity += Vector2(0, my_speed).rotated(rotation)
            if randi() % 40 == 1:
                rotation += 0.008    
        if Input.is_action_pressed("ui_down"):
            velocity -= Vector2(0, SPEED * 0.35).rotated(rotation)
            if randi() % 20 == 1:
                rotation -= 0.015
        if Input.is_action_pressed("ui_right"):
            rotation += 0.025
        if Input.is_action_pressed("ui_left"):
            rotation -= 0.025
        if Input.is_action_pressed("laugh"):
            rpc("player_emote", name, 0)
        if Input.is_action_pressed("alert"):
            rpc("player_emote", name, 1)
        if Input.is_action_pressed("heart"):
            rpc("player_emote", name, 2)
        if Input.is_action_pressed("happy"):
            rpc("player_emote", name, 3)
        if Input.is_action_pressed("money"):
            rpc("player_emote", name, 4)
        if Input.is_action_pressed("sad"):
            rpc("player_emote", name, 5)
        if Input.is_action_pressed("idea"):
            rpc("player_emote", name, 6)
        if Input.is_action_pressed("angry"):
            rpc("player_emote", name, 7)
        if Input.is_action_pressed("sleeps"):
            rpc("player_emote", name, 8)
        if Input.is_action_pressed("question"):
            rpc("player_emote", name, 9)
        if Input.is_action_pressed("nope"):
            rpc("player_emote", name, 10)
        rset_unreliable("slave_position", position)
        rset_unreliable("slave_rotation", rotation)
    else:
        position = slave_position
        rotation = slave_rotation
        
    move_and_slide(velocity * delta)
    if randi() % 20 == 1:
                var track_b = tracks.instance()
                track_b.position = position
                track_b.rotation = rotation
                get_tree().get_root().add_child(track_b)
    get_node("Emote").rotation = -rotation
    
func _hide_emote():
    get_node("Emote").visible = false
    
func set_player_name(player_name):
    get_node("Label").text = player_name
    
func emote(emote_id):
    if emote_id == 0:
        get_node("Emote").texture = laugh_texture
    if emote_id == 1:
        get_node("Emote").texture = alert_texture
    if emote_id == 2:
        get_node("Emote").texture = heart_texture
    if emote_id == 3:
        get_node("Emote").texture = happy_texture
    if emote_id == 4:
        get_node("Emote").texture = money_texture
    if emote_id == 5:
        get_node("Emote").texture = sad_texture
    if emote_id == 6:
        get_node("Emote").texture = idea_texture
    if emote_id == 7:
        get_node("Emote").texture = angry_texture
    if emote_id == 8:
        get_node("Emote").texture = sleeps_texture
    if emote_id == 9:
        get_node("Emote").texture = question_texture
    if emote_id == 10:
        get_node("Emote").texture = nope_texture
    get_node("Emote").visible = true
    emote_timer.start()