extends Control

var player_info = {}

func _ready():
    # Called every time the node is added to the scene.
    gamestate.connect("connection_succeeded", self, "_on_connection_success")

func _on_join_pressed():
    if get_node("connect/name").text == "":
        get_node("connect/error_label").text = "Invalid name!"
        return

    var ip = "35.208.43.111"
    if not ip.is_valid_ip_address():
        get_node("connect/error_label").text = "Invalid IPv4 address!"
        return

    get_node("connect/error_label").text=""
    get_node("connect/join").disabled = true

    var player_name = get_node("connect/name").text
    gamestate.join_game(ip, player_name)

func _on_connection_success():
    get_node("connect").hide()

func _on_connection_failed():
    get_node("connect/host").disabled = false
    get_node("connect/join").disabled = false
    get_node("connect/error_label").set_text("Connection failed.")

func _on_game_ended():
    show()
    get_node("connect").show()

func _on_game_error(errtxt):
    get_node("error").dialog_text = errtxt
    get_node("error").popup_centered_minsize()