extends Sprite

var hide_timer = Timer.new()

# Called when the node enters the scene tree for the first time.
func _ready():
    hide_timer.connect("timeout", self, "_hide_track")
    hide_timer.wait_time = 3
    hide_timer.one_shot = true
    add_child(hide_timer)
    hide_timer.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass
func _hide_track():
    self.queue_free()
    