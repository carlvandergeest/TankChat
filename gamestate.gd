extends Node

const DEFAULT_PORT = 10567
var player_name = "Player 1"
signal connection_succeeded()

var player = preload("res://Player.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
    get_tree().connect("connected_to_server", self, "_connected_ok")

func join_game(ip, new_player_name):
    player_name = new_player_name
    var host = NetworkedMultiplayerENet.new()
    host.create_client(ip, DEFAULT_PORT)
    get_tree().set_network_peer(host)
    
func _connected_ok():
    # We just connected to a server
    emit_signal("connection_succeeded")
    rpc("register_player", get_tree().get_network_unique_id(), self.player_name)

remote func register_player(player_id, remote_player_name):
    print("registered " + str(player_id))
    var p = player.instance()
    p.set_network_master(player_id)
    p.name = str(player_id)
    p.set_player_name(remote_player_name)
    get_tree().get_root().add_child(p)
    
remote func unregister_player(player_id):
    print("unregistered " + str(player_id))
    get_tree().get_root().get_node(str(player_id)).queue_free()
    
remote func player_emote(player_id, emote_id):
    get_tree().get_root().get_node(str(player_id)).emote(emote_id)